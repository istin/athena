#include "TrigTileMuId/TrigTileRODMuAlg.h"
#include "TrigTileMuId/TrigTileLookForMuAlg.h"
#include "TrigTileMuId/TrigTileMuToNtuple.h"
#include "TrigTileMuId/TrigTileMonAlg.h"
#include "TrigTileMuId/TrigTileMuFex.h"

DECLARE_COMPONENT( TrigTileRODMuAlg )
DECLARE_COMPONENT( TrigTileLookForMuAlg )
DECLARE_COMPONENT( TrigTileMuToNtuple )
DECLARE_COMPONENT( TrigTileMonAlg )
DECLARE_COMPONENT( TrigTileMuFex )
