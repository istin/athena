#
# File specifying the location of HEPUtils to use.
#

set( HEPUTILS_LCGVERSION 1.3.2 )
set( HEPUTILS_LCGROOT
   ${LCG_RELEASE_DIR}/MCGenerators/heputils/${HEPUTILS_LCGVERSION}/${LCG_PLATFORM} )
