#
#  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#

from AthenaMonitoring.AthMonitorCfgHelper import AthMonitorCfgHelper, AthMonitorCfgHelperOld
from AthenaMonitoring.AtlasReadyFilterTool import GetAtlasReadyFilterTool
